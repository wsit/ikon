import React, { Component } from 'react';
import * as d3 from 'd3';
import ReactDOM from 'react-dom'
import { hierarchy, pack } from 'd3-hierarchy'
const dataJson = require('./flare.json');


class ReactD3Pack extends React.Component {
    // ------------------------------
    // React lifecycle
    // ------------------------------

    // propTypes: {
    //     json: React.PropTypes.string.isRequired,
    //     startDelay: React.PropTypes.number,
    //     elementDelay: React.PropTypes.number
    // },

    // React component Mount
    componentDidMount() {
        this.initD3(this.props.json);
    }

    // Components receive new this.props
    componentWillReceivethis(props) {
        this.load(props);
    }

    

    // React UnMount
    componentWillUnmount() {
        clearInterval(this.resizeTimer);
    }


    // ------------------------------
    // D3
    // ------------------------------


    // Init D3 Layout
    initD3 = (data) => {
        var svg = d3.select(ReactDOM.findDOMNode(this)).insert("svg:svg").attr("width", 1000)
        .attr("height", 1000),
            margin = 20,
            diameter = 100,
            w = 100,
            h = 100,
            r = 100,
            g = svg.append("g").attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");
        var color = d3.scaleLinear()
            .domain([-1, 5])
            .range(["hsl(152,80%,80%)", "hsl(228,30%,40%)"])
            .interpolate(d3.interpolateHcl);
        var pack = d3.pack()
            .size([diameter - margin, diameter - margin])
            .padding(2);
            
        // d3.json("flare.json").then(function (error, root) {
            // if (error) throw error;
            var root = this.props.json;
            root = d3.hierarchy(root)
                .sum(function (d) { return d.size; })
                .sort(function (a, b) { return b.value - a.value; });
            // console.log(root, 'root');
            
            var focus = root,
                nodes = pack(root).descendants(),
                view;
            var circle = g.selectAll("circle")
                .data(nodes)
                .enter().append("circle")
                .attr("class", function (d) { return d.parent ? d.children ? "node" : "node node--leaf" : "node node--root"; })
                .attr("r", function (d) {  return (d.data.size/1000000000000)*2; })
                .attr("transform", function (d) {  
                    var width = d.data.size/100000000000;
                    var translete = "translate(" + (width)/2 +","+ (width)/2 +")"
                    return translete; 
                })
                .style("fill", function (d) { return d.children ? color(d.depth) : null; })
                // .on("click", function (d) { if (focus !== d) this.zoom(d), d3.event.stopPropagation(); });
            var text = g.selectAll("text")
                .data(nodes)
                .enter().append("text")
                .attr("class", "label")
                .style("fill-opacity", function (d) { return d.parent === root ? 1 : 0; })
                .style("display", function (d) { return d.parent === root ? "inline" : "none"; })
                .text(function (d) { return d.data.name; });
            var node = g.selectAll("circle,text");

            
            svg.style("background", color(-1))
                // .on("click", function () { this.zoom(root); });
            this.zoomTo([root.x, root.y, root.r * 2 + margin]);
        // });
    }
    zoom = (d) => {
        // var focus0 = this.focus; this.focus = d;
        // var transition = d3.transition()
        //     .duration(d3.event.altKey ? 7500 : 750)
        //     .tween("zoom", function(d) {
        //       var i = d3.interpolateZoom(this.view, [this.focus.x, this.focus.y, this.focus.r * 2 + this.margin]);
        //       return function(t) { this.zoomTo(i(t)); };
        //     });
        // transition.selectAll("text")
        //   .filter(function(d) { return d.parent === this.focus || this.style.display === "inline"; })
        //     .style("fill-opacity", function(d) { return d.parent === this.focus ? 1 : 0; })
        //     .on("start", function(d) { if (d.parent === this.focus) this.style.display = "inline"; })
        //     .on("end", function(d) { if (d.parent !== this.focus) this.style.display = "none"; });
      }
    zoomTo = (v) => {
        // var k = this.diameter / v[2]; this.view = v;
        // this.node.attr("transform", function(d) { return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")"; });
        // this.circle.attr("r", function(d) { return d.r * k; });
      }


    // Render
    render() {
        return <span></span>
    }

}

export default ReactD3Pack;
